package net.oxmc.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DBStatement {
    private String sql;

    public DBStatement(String sql) {
        this.sql = sql;
    }

    /**
     * Выполняет запрос на обновление данных в БД
     * @param params параметры запроса
     * @return количество измененных строк
     * @throws SQLException при ошибке, при этом принимается попытка закрываетия соединения, которая может вызвать SQLException
     */
    public int executeUpdate(Object... params) throws SQLException {
        Connection connection = Database.getConnection();
        if (connection == null) {
            throw new SQLException("No connection");
        }
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            for (int i = 0; i < params.length; i++) {
                statement.setObject(i + 1, params[i]);
            }
            return statement.executeUpdate();
        } finally {
            connection.close();
        }
    }

    /**
     * Выполняет запрос на получение данных в БД
     * @param params параметры запроса
     * @return массив данных, находящихся в столбцах, null, если нет данных
     * @throws SQLException при ошибке, при этом принимается попытка закрываетия соединения, которая может вызвать SQLException
     */
    public Object[] executeOneResultQuery(Object... params) throws SQLException {
        Connection connection = Database.getConnection();
        if (connection == null) {
            throw new SQLException("No connection");
        }
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            for (int i = 0; i < params.length; i++) {
                statement.setObject(i + 1, params[i]);
            }
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.first()) {
                int columnCount = resultSet.getMetaData().getColumnCount();
                Object[] data = new Object[columnCount];
                for (int i = 0; i < data.length; i++) {
                    data[i] = resultSet.getObject(i + 1);
                }
                return data;
            } else {
                return null;
            }
        } finally {
            connection.close();
        }
    }

    /**
     * Выполняет запрос на получение данных в БД
     * @param params параметры запроса
     * @return массив: array[i][j] - содержимое j столбца, i строки ответа БД
     * @throws SQLException при ошибке, при этом принимается попытка закрываетия соединения, которая может вызвать SQLException
     */
    public Object[][] executeQuery(Object... params) throws SQLException {
        Connection connection = Database.getConnection();
        if (connection == null) {
            throw new SQLException("No connection");
        }
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            for (int i = 0; i < params.length; i++) {
                statement.setObject(i + 1, params[i]);
            }
            ResultSet resultSet = statement.executeQuery();
            List<Object[]> rowsList = new ArrayList<Object[]>();
            int columnCount = resultSet.getMetaData().getColumnCount();

            while (resultSet.next()) {
                Object[] data = new Object[columnCount];
                for (int i = 0; i < data.length; i++) {
                    data[i] = resultSet.getObject(i + 1);
                }
                rowsList.add(data);
            }
            Object[][] rows = new Object[rowsList.size()][];
            for (int i = 0; i < rowsList.size(); i++) {
                rows[i] = rowsList.get(i);
            }
            return rows;
        } finally {
            connection.close();
        }
    }
}
