package net.oxmc.db;

import org.apache.commons.dbcp.BasicDataSource;
import org.bukkit.configuration.Configuration;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.logging.Level;
import java.util.logging.Logger;

public class XDatabase extends JavaPlugin {
    private Logger logger;

    @Override
    public void onDisable() {
        Database.shutdown();
    }

    public void onEnable() {
        logger = getLogger();
        getCommand("db").setExecutor(new XDBCommandHandler());

        Database.setLogger(logger);

        saveDefaultConfig();
        Configuration config = getConfig();
        String url = config.getString("url");
        String username = config.getString("username");
        String password = config.getString("password");
        String driver = config.getString("driver");

        logger.log(Level.INFO, "Loading database driver...");
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException e) {
            logger.log(Level.SEVERE, "No database driver " + driver);
            return;
        }
        logger.log(Level.INFO, "Done");


        logger.log(Level.INFO, "Creating data source");
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName(driver);
        dataSource.setUrl(url);
        dataSource.setUsername(username);
        dataSource.setPassword(password);
        Database.setDataSource(dataSource);
        logger.log(Level.INFO, "Done");
    }
}
