package net.oxmc.xprefixer;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * Занимается хранением и обработкой данных об отображаемых никах, а также коммуницацией с клиентом
 */
public class DisplayNameManager
{
    public static final String CHANNEL_PREFIX_SET = "Prefix|set";
    public static final String CHANNEL_PREFIX_RESET = "Prefix|reset";

    private Plugin plugin;
    // Сопоставляет ник игрока и его ник, отображаемый над головой
    private Map<String, String> displayNames = new HashMap<String, String>();

    public DisplayNameManager(Plugin plugin)
    {
        this.plugin = plugin;
    }

    public void init() {
        plugin.getServer().getMessenger().registerOutgoingPluginChannel(plugin, CHANNEL_PREFIX_SET);
        plugin.getServer().getMessenger().registerOutgoingPluginChannel(plugin, CHANNEL_PREFIX_RESET);
    }

    public String getDisplayName(String player)
    {
        return this.displayNames.get(player);
    }

    public void setDisplayName(String player, String displayName)
    {
        this.displayNames.put(player, displayName);
        sendUpdatedName(player);
    }

    public void setPrefix(Player player, String prefix)
    {
        if (getDisplayName(player.getName()) != null)
            return;
        setDisplayName(player.getName(), prefix == null ? null : prefix + player.getName());
    }

    public void resetDisplayName(String player)
    {
        setDisplayName(player, null);
    }

    /**
     * Сообщает клиенту игрока о никах над головой других игроков
     * @param player игрок, которому надо сообщить информацию
     */
    public void sendAllDisplayNames(Player player)
    {
        for (Player p : Bukkit.getOnlinePlayers()) {
            if (!this.displayNames.containsKey(p.getName()))
            {
                continue;
            }
            String displayName = this.displayNames.get(p.getName());

            String usedChannel = displayName != null ? CHANNEL_PREFIX_SET : CHANNEL_PREFIX_RESET;
            byte[] packetData = displayName != null ? getNameSetData(p, displayName) : getNameResetData(p);

            if (packetData != null) {
                player.sendPluginMessage(plugin, usedChannel, packetData);
            }
        }
    }

    /**
     * Сообщает всем игрокам на сервере. какой ник над головой у этого игрока
     * @param player игрок, информацию о котором надо передать клиентам
     */
    public void sendUpdatedName(String player)
    {
        String displayName = this.displayNames.get(player);

        String usedChannel = displayName != null ? CHANNEL_PREFIX_SET : CHANNEL_PREFIX_RESET;
        byte[] packetData = displayName != null ? getNameSetData(player, displayName) : getNameResetData(player);

        if (packetData != null) {
            for (Player x : Bukkit.getOnlinePlayers())
            {
                x.sendPluginMessage(this.plugin, usedChannel, packetData);
            }
        }
    }

    private byte[] getNameSetData(String player, String name) {
        return getNameSetData(Bukkit.getPlayer(player), name);
    }

    private byte[] getNameResetData(String playerName) {
        return getNameResetData(Bukkit.getPlayer(playerName));
    }

    /**
     * Формирует пакет, содержащий информацию о том, что отображаемый ник соотвествует реальному
     * @param player игрок, о нике которого говорится
     * @return содержимое пакета, null при player == null или ошибке выполнения
     */
    private byte[] getNameResetData(Player player) {
        if (player == null)
            return null;

        ByteArrayOutputStream byteBuf = new ByteArrayOutputStream();
        DataOutputStream bufOut = new DataOutputStream(byteBuf);
        try
        {
            bufOut.writeInt(player.getEntityId());
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
        return byteBuf.toByteArray();
    }

    /**
     * Формирует пакет, содержащий информацию о отображаемом нике игрока
     * @param player игрок с отображаемым ником
     * @param name его отображаемый ник
     * @return содержимое пакета, null при player == null или ошибке выполнения
     */
    private byte[] getNameSetData(Player player, String name)
    {
        if (player == null)
            return null;

        ByteArrayOutputStream byteBuf = new ByteArrayOutputStream();
        DataOutputStream bufOut = new DataOutputStream(byteBuf);
        try
        {
            byte[] displayNameBytes = name.getBytes("UTF8");
            bufOut.writeInt(player.getEntityId());
            bufOut.writeInt(displayNameBytes.length);
            bufOut.write(displayNameBytes);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
        return byteBuf.toByteArray();
    }
}