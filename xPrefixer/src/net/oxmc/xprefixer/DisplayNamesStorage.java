package net.oxmc.xprefixer;

import net.oxmc.db.DBStatement;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.sql.SQLException;

/**
 * Работает с данными в БД
 */
public class DisplayNamesStorage {
    // SQL-запрос получения отображаемого имени по имени игрока
    private DBStatement queryStatement;
    // SQL-запрос установки отображаемого имени игроку
    private DBStatement addStatement;
    // SQL-запрос создание таблицы
    private DBStatement createStatement;

    public DisplayNamesStorage() throws IOException {
        queryStatement = new DBStatement(readSQLFromResource("/sql/query.sql"));
        addStatement = new DBStatement(readSQLFromResource("/sql/add.sql"));
        createStatement = new DBStatement(readSQLFromResource("/sql/create.sql"));
    }

    /**
     * Создает таблицу
     */
    public void init() throws StorageCommunicationException {
        try {
            createStatement.executeUpdate();
        } catch (SQLException e) {
            throw new StorageCommunicationException("Error creating display names table", e);
        }
    }

    /**
     * Достает из БД отображаемый ник
     * @param playerName игрок
     * @return отображаемый ник, null, если нет данных в БД
     */
    public String loadDisplayName(String playerName) throws StorageCommunicationException {
        try {
            Object[] results = queryStatement.executeOneResultQuery(playerName);
            return (String) results[0];
        } catch (SQLException e) {
            throw new StorageCommunicationException("Error loading display name for " + playerName, e);
        }
    }

    /**
     * Записывает в БД соотвествие (ник игрока - отображаемый ник)
     * @param playerName ник игрока
     * @param displayName отображаемый ник
     */
    public void storeDisplayName(String playerName, String displayName) throws StorageCommunicationException {
        try {
            addStatement.executeUpdate(playerName, displayName, displayName);
        } catch (SQLException e) {
            throw new StorageCommunicationException("Error storing display name for " + playerName, e);
        }
    }

    /**
     * Достает инструкции SQL из файла
     */
    private String readSQLFromResource(String resource) throws IOException {
        InputStream inputStream = null;
        try {
            inputStream = getClass().getResourceAsStream(resource);
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, Charset.forName("UTF8")));
            StringBuilder contents = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                contents.append(line);
                contents.append('\n');
            }
            return contents.toString();
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
    }
}
